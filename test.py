import sys
import datetime
import random
import string
import argparse
import json
import os

currentDirectory = os.getcwd() # Permet de preciser ou chercher le dossier 


def password_generator(length=10):
    chars = string.ascii_letters + string.digits
    password = ""
    for i in range(0, length):
        char = random.choice(chars)
        password = password + char
    return password

def animals_generator():
    animals = ""
        
    with open(currentDirectory + '/animals.txt','r') as first_word: 
        lignes1 = first_word.readlines()
        animals = random.choice(lignes1).strip()
    return animals

def adjectives_generator():
    adjectives = ""
    with open(currentDirectory + '/english-adjectives.txt','r') as second_word:
        lignes2 = second_word.readlines()
        adjectives = random.choice(lignes2).strip()
    return adjectives

def user_generator(length):
    login = {
            "username" : animals_generator() + adjectives_generator(),
            "password" : password_generator(length)
            }
    return login

if __name__ == "__main__":
    pass
