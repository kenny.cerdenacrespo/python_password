import argparse
import json
import test
import pathlib


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--length", help = "Defini la longueur du Mdp", action="store", type=int, default=10)
    parser.add_argument("-u", "--user", help = "Defini le nombre de user", action="store", type=int, default=1)
    args = parser.parse_args()
    
    user = args.user
    length = args.length
    
    all_logins = []
    
    if pathlib.Path("data_file.json").is_file():
        with open ("data_file.json", "r") as user_file:
            data = json.load(user_file)
            all_logins.extend(data)
   
    for i in range(0, args.user):
        login = test.user_generator(args.length)
        all_logins.append(login)

    print(all_logins)

    with open ("data_file.json", "w") as user_file:
        json.dump(all_logins, user_file)
    
    exit()
